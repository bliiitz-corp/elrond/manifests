elrond-node
===========
Helm chart for deploy elrond node(s) with monitoring tools

Current chart version is `0.1.0`

Source code can be found [here](https://github.com/elrondnetwork/elrond-go)

## Usage

You just have to set the list of `initial_balances_sk` and `initial_nodes_sk` pair in `elrond_node_keys` in values.yaml
It will deploy one node by entity in the list with a prometheus/grafana stack witch monitor thes nodes

Grafana & Prometheus isn't exposed with ingress or LoadBalancer service.

The only one method to connect to Grafana interface is `kubectl port-forward`

Exemple: `kubectl port-forward -n <elrond-namespace> svc/grafana 3000:3000` 

Default admin login of grafana is grafana/grafana

## Chart Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| elrond_node.image | string | `"registry.gitlab.com/bliiitz-corp/elrond/docker-image"` |  |
| elrond_node.version | string | `"v1.0.101"` |  |
| elrond_node_exporter.image | string | `"registry.gitlab.com/bliiitz-corp/elrond/elrond-node-exporter"` |  |
| elrond_node_exporter.version | string | `"v1.0"` |  |
| elrond_node_keys[].initial_balances_sk | string | `"### your initial balance sk PEM file content\n"` |  |
| elrond_node_keys[].initial_nodes_sk | string | `"### your initial node sk PEM file content\n"` |  |
| grafana.image | string | `"registry.gitlab.com/bliiitz-corp/elrond/grafana"` |  |
| grafana.version | string | `"latest"` |  |
| namespace | string | `"elrond-network"` |  |
| node_name_prefix | string | `"my"` |  |
| prometheus.image | string | `"prom/prometheus"` |  |
| prometheus.version | string | `"v2.4.3"` |  |
